test('Boundaries', function (assert) {
  assert.equal(isValidPlate('0000BBB'), true, 'LOWER_LIMIT');
  assert.equal(isValidPlate('9999ZZZ'), true, 'UPPER_LIMIT');
});

test('Valid Uppercase', function (assert) {
  assert.equal(isValidPlate('0123CDF'), true, 'VALID UPPERCASE');
  assert.equal(isValidPlate('4567GHJ'), true, 'VALID UPPERCASE');
  assert.equal(isValidPlate('8901KLM'), true, 'VALID UPPERCASE');
  assert.equal(isValidPlate('2389NPR'), true, 'VALID UPPERCASE');
  assert.equal(isValidPlate('6745STV'), true, 'VALID UPPERCASE');
  assert.equal(isValidPlate('1076WXY'), true, 'VALID UPPERCASE');
  assert.equal(isValidPlate('3210ZBC'), true, 'VALID UPPERCASE');
});

test('Valid Lowercase', function (assert) {
  assert.equal(isValidPlate('2323fcd'), true, 'VALID LOWERCASE');
  assert.equal(isValidPlate('7777jgh'), true, 'VALID LOWERCASE');
  assert.equal(isValidPlate('9998mkl'), true, 'VALID LOWERCASE');
  assert.equal(isValidPlate('1144rnp'), true, 'VALID LOWERCASE');
  assert.equal(isValidPlate('5782vst'), true, 'VALID LOWERCASE');
  assert.equal(isValidPlate('6662yzb'), true, 'VALID LOWERCASE');
  assert.equal(isValidPlate('8869xwc'), true, 'VALID LOWERCASE');
});

test('Mispositioned: Supported chars but not in the right place', function (assert) {
  assert.equal(isValidPlate('A444BBB'), false, 'LETTER IN 1st CHAR');
  assert.equal(isValidPlate('5B77CDF'), false, 'LETTER IN 2nd CHAR');
  assert.equal(isValidPlate('88C9KTW'), false, 'LETTER IN 3rd CHAR');
  assert.equal(isValidPlate('000ZRST'), false, 'LETTER IN 4th CHAR');
  assert.equal(isValidPlate('67451PP'), false, 'NUMBER IN 5th CHAR');
  assert.equal(isValidPlate('1076W0W'), false, 'NUMBER IN 6th CHAR');
  assert.equal(isValidPlate('3210AA9'), false, 'NUMBER IN 7th CHAR');
});

test('Letter exceptions: not valid letters for last 3 chars', function (assert) {
  assert.equal(isValidPlate('1235ASF'), false, 'A IN PLATE LETTERS');
  assert.equal(isValidPlate('0990BEZ'), false, 'E IN PLATE LETTERS');
  assert.equal(isValidPlate('6666BBI'), false, 'I IN PLATE LETTERS');
  assert.equal(isValidPlate('7877ZOZ'), false, 'O IN PLATE LETTERS');
  assert.equal(isValidPlate('1441PPU'), false, 'U IN PLATE LETTERS');
  assert.equal(isValidPlate('2989QTT'), false, 'Q IN PLATE LETTERS');
  assert.equal(isValidPlate('3444WÑW'), false, 'Ñ IN PLATE LETTERS');
});

test('Invalid chars', function (assert) {
  assert.equal(isValidPlate('.232FSF'), false, 'A IN PLATE LETTERS');
  assert.equal(isValidPlate('0=90BBZ'), false, 'E IN PLATE LETTERS');
  assert.equal(isValidPlate('65 6BBK'), false, 'I IN PLATE LETTERS');
  assert.equal(isValidPlate('780*ZZZ'), false, 'O IN PLATE LETTERS');
  assert.equal(isValidPlate('1111$PB'), false, 'U IN PLATE LETTERS');
  assert.equal(isValidPlate('2229T;T'), false, 'Q IN PLATE LETTERS');
  assert.equal(isValidPlate('3424Wc:'), false, 'Ñ IN PLATE LETTERS');
});
