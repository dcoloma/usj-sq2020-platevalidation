# usj-sq2020-platevalidation

CRITERIA FOR SELECTING TEST CASES

Let's break it down into two main output domains:

1. Valid Plates: For valid plates we will try to test the limits and at least one time each of the possible valid values (numbers from 0 to 9 and all the letters except the forbidden ones)
2. Invalid Plates: For invalid plates we could try to distinguish the following situations:
* The user has typed characters that are valid but not in the right position (letters in the first 4 positions or numbers in the last 3). We will try to test that this is detected in all the seven positions.
* The user has entered some of the letters that are not allowed in the letter positions (last 3 positions). We will try to test that all the exceptions are successfully spotted.
* The user has entered one character outside of the supported ones. We will try to test that this is detected in all the 7 possible situations.

Please note that we have not tested Strings bigger than 7 chars as this supposed to be filtered by the UI.

The implementation of the tests is available at: https://gitlab.com/dcoloma/usj-sq2020-platevalidation/-/blob/master/tests.js